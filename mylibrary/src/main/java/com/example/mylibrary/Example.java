package com.example.mylibrary;

import android.util.Log;

public class Example {

    public static void test(){
        Log.v("MY AEWSOME LIBRARY", "method from library called"); //Verbose
        Log.d("MY AEWSOME LIBRARY", "method from library called"); //Debug
        Log.i("MY AEWSOME LIBRARY", "method from library called"); //Info
        Log.w("MY AEWSOME LIBRARY", "method from library called"); //Warning
        Log.e("MY AEWSOME LIBRARY", "method from library called"); //Error
        Log.wtf("MY AEWSOME LIBRARY", "method from library called"); //WTF
    }

}
